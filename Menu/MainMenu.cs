﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoParking.ParkingData;
using AutoParking.Vehicles;

namespace AutoParking.Menu
{
    public class MainMenu
    {
        static Parking parking = Parking.GetInstance;

        const string CurrentParkingBalance = "Current Parking Balance";
        const string TheAmountMmoneyEarnedInTheLastMinute = "The amount of money earned in the last minute";
        const string FindOutTheNumberOfFreeOccupiedParkingPlaces = "Find out the number of free / occupied parking places";
        const string DisplayAllParkingTransactionsForLastMinute = "Display all Parking Transactions for the last minute";
        const string PrintTheEntireHistoryOfTransactions = "Print the entire history of transactions";
        const string DisplayListOfAllVehicles = "Display a list of all Vehicles";
        const string RemovePickUpVehicleFromParking = "Remove / pick up Vehicle from Parking";
        const string CreateDeliverVehicleParking = "Create / deliver Vehicle to Parking";
        const string TopUpTheBalanceOfParticularVehicle = "Top up the balance of a particular Vehicle";
        delegate void CurrentMenu();

        static IDictionary<string, CurrentMenu> _mainMenuAction = new Dictionary<string, CurrentMenu>
        {
            { CurrentParkingBalance, () => ShowMenu(new Dictionary<string, CurrentMenu>{{ Params.Exit, null }}, $"{CurrentParkingBalance} is {parking.GetBalance()}")  },
            {TheAmountMmoneyEarnedInTheLastMinute, () => ShowMenu(new Dictionary<string, CurrentMenu>{{ Params.Exit, null }}, $"{TheAmountMmoneyEarnedInTheLastMinute} is {parking.GetBalanceViaMinute()}")  },
            {FindOutTheNumberOfFreeOccupiedParkingPlaces, () => ShowMenu(new Dictionary<string, CurrentMenu>{{ Params.Exit,  null }}, $"Free place is equal {parking.GetFreePlaceCount}")  },
            {DisplayAllParkingTransactionsForLastMinute, () => ShowMenu(new Dictionary<string, CurrentMenu>{{ Params.Exit, null }}, $"{DisplayAllParkingTransactionsForLastMinute} {parking.GetTransactionsViaMinuteToString()}")  },
            {PrintTheEntireHistoryOfTransactions, () => ShowMenu(new Dictionary<string, CurrentMenu>{{ Params.Exit, null }}, $"{PrintTheEntireHistoryOfTransactions} {parking.GetTransactionsViaMinuteToString()}")  },
            {DisplayListOfAllVehicles, () => ShowMenu(new Dictionary<string, CurrentMenu>{{ Params.Exit, null }}, $"{DisplayListOfAllVehicles} {parking.GetAllVehiclesToString()}")  },
            {CreateDeliverVehicleParking, () => ShowMenu(_vehicleCreate, CreateDeliverVehicleParking) },
            {RemovePickUpVehicleFromParking, () => ShowMenu(VehicleDelete(), RemovePickUpVehicleFromParking, VehicleDelete)},
            {TopUpTheBalanceOfParticularVehicle, () => ShowMenu(AddBalance(), TopUpTheBalanceOfParticularVehicle, AddBalance)},
            {Params.Exit, null}
        };

        static IDictionary<string, CurrentMenu> _vehicleCreate = new Dictionary<string, CurrentMenu>()
        {
            { Params.Motorcycle, () => parking.AddVehicle(new Motorcycle()) },
            { Params.PassengerCar, () => parking.AddVehicle(new PassengerCar()) },
            { Params.Bus, () => parking.AddVehicle(new Bus()) },
            { Params.Truck, () => parking.AddVehicle(new Truck()) },
            { Params.Exit, null },
        };


        static IDictionary<string, CurrentMenu> VehicleDelete()
        {
            CurrentMenu GetDelegate(Vehicle x) => () => parking.GetVehicle(x.VehicleNumber);

            IDictionary<string, CurrentMenu> vehicleDelete = parking.GetAllVehicles
                .ToDictionary(x => x.VehicleId, GetDelegate);

            vehicleDelete.Add(Params.Exit, null);
            return vehicleDelete;
        }
       static double GetMoney()
        {
            do
            {
                try
                {
                    Console.WriteLine("enter extra money");
                    string str = Console.ReadLine();
                    return double.Parse(str);
                }
                catch (Exception e)
                {
                    Console.WriteLine("enter just number");
                }

            } while (true);
        }
        static IDictionary<string, CurrentMenu> AddBalance()
        {

            CurrentMenu GetDelegate(Vehicle x) => () => parking.AddBalance(x.VehicleNumber, GetMoney());

            IDictionary<string, CurrentMenu> vehicleDelete = parking.GetAllVehicles
                .ToDictionary(x => x.VehicleId, GetDelegate);

            vehicleDelete.Add(Params.Exit, null);
            return vehicleDelete;
        }

        public void Run()
        {
            ShowMenu(_mainMenuAction, "main menu");
        }

        static void ShowMenu(IDictionary<string, CurrentMenu> dictionary, string title, Func<IDictionary<string, CurrentMenu>> forUptudate = null)
        {
            List<string> list = dictionary.Keys.ToList();
            string executed = string.Empty;
            do
            {
                if (forUptudate != null)
                {
                    list = forUptudate().Keys.ToList();
                }
                ShowMenu(title, list);
                if (!string.IsNullOrEmpty(executed))
                    Console.WriteLine($"executed #{executed}");

                int number = GetNumberOfMenu(list.Count);
                var str = list[number];
                if (str == Params.Exit)
                    return;
                try
                {
                    dictionary[str]?.Invoke();
                    executed = number.ToString();
                }
                catch (Exception e)
                {
                    executed = number + " with Exception:" + e.Message;
                }

            } while (true);
        }

        private static int GetNumberOfMenu(int count)
        {
            string str = string.Empty;
            int result = -1;
            do
            {
                Console.WriteLine("Choice your variant");
                Console.WriteLine("");
                str = Console.ReadLine();
                try
                {
                    result = int.Parse(str);
                    if (result >= 0 && result <= count)
                    {
                        return result;
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Enter just number");
                }
                Console.WriteLine("Enter correct number of menu");

            } while (true);
        }

        private static void ShowMenu(string title, ICollection<string> menu)
        {
            Console.Clear();
            Console.WriteLine(title.ToUpper());
            int count = 0;
            foreach (var item in menu)
            {
                Console.WriteLine(count++ + " - " + item);
            }
        }
    }
}