﻿using System;
using AutoParking.Menu;
using AutoParking.Vehicles;

namespace AutoParking
{
    class Program
    {
        static void Main(string[] args)
        {
            new MainMenu().Run();
        }
    }
}
