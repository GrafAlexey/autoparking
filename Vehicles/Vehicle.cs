﻿using System;
using AutoParking.Infrastructure;

namespace AutoParking.Vehicles
{
    public abstract class Vehicle
    {
        public int VehicleNumber { get; private set; }
        public string VehicleType { get; private set; }
        public string VehicleId { get; private set; }
        public double Balance { get; private set; }

        protected Vehicle(string type, double balance)
        {
            this.VehicleType = type;
            this.Balance = balance;
            this.VehicleNumber = this.GetHashCode();
            this.VehicleId = $"{this.VehicleType} {this.VehicleNumber}";
        }
        public void AddBalance(double money)
        {
            if (money >= 0)
            {
                Balance += money;
            }
            else
            {
                throw new Exception($"{nameof(money)} cannot be negative");
            }
        }
        public double WithdrawBalance(double money)
        {
            if (money >= 0)
            {
                if (Balance >= money)
                {
                    Balance -= money;
                    return money;
                }

                return 0d;
            }

            throw new Exception($"{nameof(money)} cannot be negative");
        }
        public override string ToString()
        {
            return VehicleId;
        }
    }
}
