﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoParking.Vehicles
{
    public class PassengerCar : Vehicle
    {
        public PassengerCar() : base(Params.PassengerCar, Params.VehicleBalance[Params.PassengerCar])
        {
        }
    }
}
