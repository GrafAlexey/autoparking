﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoParking.Vehicles
{
    public class Motorcycle : Vehicle
    {
        public Motorcycle() : base(Params.Motorcycle, Params.VehicleBalance[Params.Motorcycle])
        {
        }
    }
}
