﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoParking.Vehicles
{
    public class Truck : Vehicle
    {
        public Truck() : base(Params.Truck, Params.VehicleBalance[Params.Truck])
        {
        }
    }
}
