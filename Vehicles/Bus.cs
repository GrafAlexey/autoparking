﻿namespace AutoParking.Vehicles
{
    public class Bus : Vehicle
    {
        public Bus() : base(Params.Bus, Params.VehicleBalance[Params.Bus])
        {
        }
    }
}
