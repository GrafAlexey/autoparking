﻿using System.Collections.Generic;

namespace AutoParking
{
    public static class Params
    {
        public const string Exit = "Exit";
        public const string Motorcycle = "Motorcycle";
        public const string PassengerCar = "Passenger Car";
        public const string Bus = "Bus";
        public const string Truck = "Truck";
        public const int ParkingCapacity = 10;
        public const int ParkingTaxFrequency = 5000;
        public const double ParkingBalance = 0d;
        public const double ParkingPenaltyCoefficient = 2.5d;
        public const double ParkingFine = 2.5d;

        public static readonly IDictionary<string, double> ParkingRates = new Dictionary<string, double>
        {
            {Motorcycle, 1 },
            {PassengerCar, 2 },
            {Bus, 3.5 },
            {Truck, 5 },
        };
        public static readonly IDictionary<string, double> VehicleBalance = new Dictionary<string, double>
        {
            {Motorcycle, 10d },
            {PassengerCar, 20d },
            {Bus, 30d },
            {Truck, 40d },
        };
    }
}
