﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using AutoParking.Vehicles;

namespace AutoParking.ParkingData
{
    public class Parking
    {
        private static Parking parking = null;
        private static Timer aTimer;
        private static Bookkeeping _bookkeeping = new Bookkeeping();
        private static List<Vehicle> _vehicles = new List<Vehicle>(Params.ParkingCapacity);

        static Parking() { }

        private static Parking Init()
        {
            Parking parkingInstance = new Parking();
            SetTimer();

            return parkingInstance;
        }
        private static void SetTimer()
        {
            aTimer = new Timer(Params.ParkingTaxFrequency);
            aTimer.Elapsed += OnEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        static void OnEvent(object s, ElapsedEventArgs e)
        {
            foreach (var vehicle in _vehicles)
            {
                _bookkeeping.GetTax(vehicle);
            }
        }
        private static void CheckInstance()
        {
            if (parking == null)
            {
                throw new Exception("at first you should get instance of parking");
            }
        }
        public static Parking GetInstance => parking ?? (parking = Init());

        public double GetBalance()
        {
            CheckInstance();

            return _bookkeeping.GetBalance;
        }

        public double GetBalanceViaMinute()
        {
            CheckInstance();

            return _bookkeeping.GetBalanceViaMinute;
        }

        public void AddBalance(int vehicleNumber, double sum)
        {
            CheckInstance();

            var item = _vehicles.FirstOrDefault(x => x.VehicleNumber == vehicleNumber);
            item?.AddBalance(sum);
            _bookkeeping.IsDebtor(item);
        }

        public void AddVehicle(Vehicle vehicle)
        {
            CheckInstance();

            if (_vehicles.Count < Params.ParkingCapacity)
            {
                _vehicles.Add(vehicle);
            }
            else
            {
                throw new Exception("on this parking does not have any place");
            }
        }

        public int GetFreePlaceCount => Params.ParkingCapacity - _vehicles.Count;
        public List<string> GetTransactionsViaMinute =>_bookkeeping.GetTransactionsViaMinute;
        public List<Vehicle> GetAllVehicles =>_vehicles;

        private string ConvertListToString(List<string> data)
        {
            data.Insert(0, "");
            data.Add("");
            data.Add("");

            return string.Join("\n\r", data);
        }

        public string GetTransactionsViaMinuteToString() => ConvertListToString(GetTransactionsViaMinute);
        public string GetAllVehiclesToString() => ConvertListToString(GetAllVehicles.Select(x=>x.ToString()).ToList());

        public void GetVehicle(int vehicleNumber)
        {
            CheckInstance();

            if (_vehicles.FirstOrDefault(x => x.VehicleNumber == vehicleNumber) is Vehicle vehicle)
            {
                if (!_bookkeeping.IsDebtor(vehicle))
                {
                    _vehicles.Remove(vehicle);
                }
                else
                {
                    throw new Exception($"The vehicle has dept: {_bookkeeping.GetDebt(vehicle)}. You should put extra money on your vehicle balance");
                }
            }
            else
            {
                throw new Exception($"The vehicle: {vehicleNumber} does not contain at the Parking");
            }
        }
    }
}
