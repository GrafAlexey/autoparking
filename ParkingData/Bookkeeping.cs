﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using AutoParking.Vehicles;

namespace AutoParking.ParkingData
{
    public class Bookkeeping
    {
        private readonly IDictionary<DateTime, (Vehicle vehicle, double money)> _transactions = new ConcurrentDictionary<DateTime, (Vehicle vehicle, double money)>();
        private readonly IDictionary<int, double> _debtors = new Dictionary<int, double>();
        private double GetMoney(Vehicle vehicle)
        {
            double rate = Params.ParkingRates[vehicle.VehicleType];

            return rate;
        }
        public void GetTax(Vehicle vehicle)
        {
            var rate = Params.ParkingRates[vehicle.VehicleType];
            var money = vehicle.WithdrawBalance(rate);
            if (Math.Abs(rate - money) >= 0.00001)
            {
                AddDebtor(vehicle, rate);
            }
            else
            {
                AddMoney(vehicle, rate);
            }
        }

        public bool IsDebtor(Vehicle vehicle)
        {
            if (_debtors.ContainsKey(vehicle.VehicleNumber))
            {
                double dobt = _debtors[vehicle.VehicleNumber];
                double sum = vehicle.WithdrawBalance(dobt);
                if (Math.Abs(sum - dobt) <= 0.000001)
                {
                    _debtors.Remove(vehicle.VehicleNumber);
                    AddMoney(vehicle, sum);
                    return false;
                }

                return true;
            }

            return false;
        }

        public double GetDebt(Vehicle vehicle) =>
            IsDebtor(vehicle) && _debtors.TryGetValue(vehicle.VehicleNumber, out double debt)
                ? debt
                : 0d;

        public void AddMoney(Vehicle vehicle, double money)
        {
            _transactions.TryAdd(DateTime.Now, (vehicle, money));
        }

        public void AddDebtor(Vehicle vehicle, double rate)
        {
            var fine = Params.ParkingFine * rate;
            if (_debtors.ContainsKey(vehicle.VehicleNumber))
            {
                _debtors[vehicle.VehicleNumber] += fine;
            }
            else
            {
                _debtors.TryAdd(vehicle.VehicleNumber, fine);
            }
        }

        public double GetBalance =>
            _transactions
            .Values
            .Select(x => x.money)
            .Sum();
        public double GetBalanceViaMinute =>
            _transactions
            .Where(IsLastMinute)
            .Select(x => x.Value.money)
            .Sum();

        public List<string> GetTransactionsViaMinute =>
            _transactions
            .Where(IsLastMinute)
            .Select(DataToString)
            .ToList();

        private bool IsLastMinute(KeyValuePair<DateTime, (Vehicle, double)> x) => x.Key > DateTime.Now.AddMinutes(-1);
        private string DataToString(KeyValuePair<DateTime, (Vehicle vehicle, double money)> x) => $"{x.Key:HH:mm:ss.fff} - {x.Value.money} - {x.Value.vehicle}";
    }
}
