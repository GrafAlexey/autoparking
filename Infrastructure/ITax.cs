﻿namespace AutoParking.Infrastructure
{
    public interface ITax
    {
        double GetTax(double money);
    }
}